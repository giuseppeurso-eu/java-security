package eu.giuseppeurso.security.otp;

import org.apache.commons.codec.binary.Base32;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Credits to: https://github.com/BastiaanJansen/otp-java
 */
public class OTPGenerator {

    // OTPAUTH URI keys
    private static final String URL_SCHEME = "otpauth";
    private static final String OTP_TYPE = "totp";
    private static final String DIGITS = "digits";
    private static final String SECRET = "secret";
    private static final String ALGORITHM = "algorithm";
    private static final String PERIOD = "period";
    private static final String ISSUER = "issuer";

    // DEFAULT Settings
    private static HashAlgorithm algorithm = HashAlgorithm.SHA1;
    private static int passwordLength = 6;
    private static Duration period = Duration.ofSeconds(30);
    private static Clock clock = Clock.system(ZoneId.systemDefault());

    private static byte[] secret;
    private static URI otpAuthURI;


    /**
     * Getters/Setters
     */
    public byte[] getSecret() {
        return secret;
    }
    public void setSecret(byte[] secret) {
        OTPGenerator.secret = secret;
    }
    public HashAlgorithm getAlgorithm() {
        return algorithm;
    }
    public void setAlgorithm(HashAlgorithm algorithm) {
        OTPGenerator.algorithm = algorithm;
    }
    public int getPasswordLength() {
        return passwordLength;
    }
    public void setPasswordLength(int passwordLength) {
        OTPGenerator.passwordLength = passwordLength;
    }
    public Duration getPeriod() {
        return period;
    }
    public void setPeriod(Duration period) {
        OTPGenerator.period = period;
    }
    public URI getOtpAuthURI() {
        return otpAuthURI;
    }
    public void setOtpAuthURI(URI otpAuthURI) {
        OTPGenerator.otpAuthURI = otpAuthURI;
    }

    /**
     * HMAC algorithm enum
     */
    public enum HashAlgorithm {

        @Deprecated
        MD4("HmacMD4"),
        @Deprecated
        MD5("HmacMD5"),
        @Deprecated
        SHA1("HmacSHA1"),
        SHA224("HmacSHA224"),
        SHA256("HmacSHA256"),
        SHA384("HmacSHA384"),
        SHA512("HmacSHA512");

        private final String name;
        HashAlgorithm(String name) {
            this.name = name;
        }
        public String getHMACName() {return name;}
    }

    /**
     * Create a TOTPGenerator with the given secret and default settings
     * @param secret
     */
    public OTPGenerator(byte[] secret) throws URISyntaxException {
        if (secret.length == 0)
            throw new IllegalArgumentException("Secret must not be empty");

        this.secret = secret;
        if(this.otpAuthURI==null){
            this.otpAuthURI = createOtpAuthURI("das","accountName");
        }
    }

    /**
     * Create a TOTPGenerator with custom settings
     * @param secret
     */
    public OTPGenerator(byte[] secret, HashAlgorithm algorithm, int passwordLength, Duration period) throws URISyntaxException {
        this.secret = secret;
        this.algorithm = algorithm;
        this.passwordLength = passwordLength;
        this.period = period;
        if(this.otpAuthURI==null){
            this.otpAuthURI = createOtpAuthURI("das","accountName");
        }
    }

    /**
     * Generate an OTP base32 secret
     *
     * @param bits length, this should be greater than or equal to the length of the HMAC algorithm type:
     *             SHA1: 128 bits
     *             SHA256: 256 bits
     *             SHA512: 512 bits
     * @return generated secret
     */
    public static byte[] generate(final int bits) {
        if (bits < 128)
            throw new IllegalArgumentException("Bits must be greater than or equal to 128");

        SecureRandom random = new SecureRandom();
        Base32 encoder = new Base32();

        byte[] bytes = new byte[bits / Byte.SIZE];
        random.nextBytes(bytes);
        return encoder.encode(bytes);
    }

    /**
     * Build a TOPGenerator from the given otpAuth URI
     * @param uri
     * @return
     * @throws URISyntaxException
     */
    public static OTPGenerator fromURI(URI uri) throws URISyntaxException {
        Map<String, String> query = queryItems(uri);

        secret = Optional.ofNullable(query.get(SECRET))
                .map(String::getBytes)
                .orElseThrow(() -> new IllegalArgumentException("Secret query parameter must be set"));

        try {

            Duration p = Optional.ofNullable(query.get(PERIOD))
                    .map(Long::parseLong)
                    .map(Duration::ofSeconds).get();
            if (p.getSeconds() < 30) throw new IllegalArgumentException("Period must be at least 30 seconds");
            period=p;

            int d = Optional.ofNullable(query.get(DIGITS))
                    .map(Integer::valueOf).get();
            boolean isValidPassLength = (d >= 6 && d <= 8) ? true : false;
            if (!isValidPassLength)
                throw new IllegalArgumentException("Password length must be between 6 and 8 digits");
            passwordLength = d;

            HashAlgorithm a = Optional.ofNullable(query.get(ALGORITHM))
                    .map(String::toUpperCase)
                    .map(HashAlgorithm::valueOf).get();
            algorithm = a;

            String uriPath = uri.getPath();

            String issuer;
            String account=null;
            if(uriPath.indexOf(":")>0){
                String[] parts = uriPath.split(":");
                issuer=parts[0].replaceAll("/","");
                account=parts[1];
            }else {
                issuer=uriPath;
            }
            if(issuer==null) new IllegalArgumentException("Issuer query parameter must be set");
            createOtpAuthURI(issuer,account);
        } catch (Exception e) {
            throw new URISyntaxException(uri.toString(), "URI could not be parsed");
        }
        return new OTPGenerator(secret);
    }

    /**
     * Get a map of query items from URI
     *
     * @param uri to get query items from
     * @return map of query items from URI
     */
    public static Map<String, String> queryItems(URI uri) {
        Map<String, String> items = new LinkedHashMap<>();
        String query = uri.getQuery();
        String[] pairs = query.split("&");

        for (String pair: pairs) {
            int index = pair.indexOf("=");
            try {
                items.put(
                        URLDecoder.decode(pair.substring(0, index), StandardCharsets.UTF_8.toString()),
                        URLDecoder.decode(pair.substring(index + 1), StandardCharsets.UTF_8.toString())
                );
            } catch (UnsupportedEncodingException e) {
                throw new IllegalStateException("Encoding should be supported");
            }
        }
        return items;
    }


    /**
     * URL Encoder
     * @param value
     * @return
     */
    public static String encodeURL(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Generate a otpAuth URI.
     * Example: otpauth://totp/companyFoo:joe?period=30&digits=6&secret=AHZEYVMYLK8BRUNA&issuer=companyFoo&algorithm=SHA1
     * @param issuer
     * @param account
     * @return
     * @throws URISyntaxException
     */
    public static URI createOtpAuthURI(final String issuer, final String account) throws URISyntaxException {

        Map<String, String> query = new HashMap<>();
        query.put(PERIOD, String.valueOf(period.getSeconds()));
        query.put(DIGITS, String.valueOf(passwordLength));
        query.put(ALGORITHM, algorithm.name());
        query.put(SECRET, new String(secret, StandardCharsets.UTF_8));
        query.put(ISSUER, issuer);

        String path = account.isEmpty() ? encodeURL(issuer) : String.format("%s:%s", encodeURL(issuer), encodeURL(account));
        String uriString = String.format("%s://%s/%s?", URL_SCHEME, OTP_TYPE, path);

        String uri = query.keySet().stream()
                .map(key -> String.format("%s=%s", key, encodeURL(query.get(key))))
                .collect(Collectors.joining("&", uriString, ""));

        otpAuthURI=new URI(uri);
        return otpAuthURI;
    }


    /**
     * Hash
     * @param secret
     * @param data
     * @return
     * @throws InvalidKeyException
     * @throws NoSuchAlgorithmException
     */
    private byte[] generateHash(final byte[] secret, final byte[] data) throws InvalidKeyException, NoSuchAlgorithmException {
        // Create a secret key with correct SHA algorithm
        SecretKeySpec signKey = new SecretKeySpec(secret, "RAW");
        // Mac is 'message authentication code' algorithm (RFC 2104)
        Mac mac = Mac.getInstance(algorithm.getHMACName());
        mac.init(signKey);
        // Hash data with generated sign key
        return mac.doFinal(data);
    }

    private String getCodeFromHash(final byte[] hash) {
        /* Find mask to get last 4 digits:
        1. Set all bits to 1: ~0 -> 11111111 -> 255 decimal -> 0xFF
        2. Shift n (in this case 4, because we want the last 4 bits) bits to left with <<
        3. Negate the result: 1111 1100 -> 0000 0011
         */
        int mask = ~(~0 << 4);

        /* Get last 4 bits of hash as offset:
        Use the bitwise AND (&) operator to select last 4 bits
        Mask should be 00001111 = 15 = 0xF
        Last byte of hash & 0xF = last 4 bits:
        Example:
        Input: decimal 219 as binary: 11011011 &
        Mask: decimal 15 as binary:   00001111
        -----------------------------------------
        Output: decimal 11 as binary: 00001011
         */
        byte lastByte = hash[hash.length - 1];
        int offset = lastByte & mask;

        // Get 4 bytes from hash from offset to offset + 3
        byte[] truncatedHashInBytes = { hash[offset], hash[offset + 1], hash[offset + 2], hash[offset + 3] };

        // Wrap in ByteBuffer to convert bytes to long
        ByteBuffer byteBuffer = ByteBuffer.wrap(truncatedHashInBytes);
        long truncatedHash = byteBuffer.getInt();

        // Mask most significant bit
        truncatedHash &= 0x7FFFFFFF;

        // Modulo (%) truncatedHash by 10^passwordLength
        truncatedHash %= Math.pow(10, passwordLength);

        // Left pad with 0s for an n-digit code
        return String.format("%0" + passwordLength + "d", truncatedHash);
    }

    /**
     * Calculate the OTP counter basing on the current instant (with time-zone) and the given interval duration.
     *
     * @param period
     * @return
     */
    private long calculateTimeBasedCounter(final Instant instant, final Duration period) {
        long now = instant.toEpochMilli(); //current instant in mills
        return  now / period.toMillis();
    }

    /**
     * Generate OTP code with counter corresponding the current instant (in millis)
     * @return
     * @throws IllegalStateException
     */
    public String generateOtpNow() throws IllegalStateException {
        long counter = calculateTimeBasedCounter(clock.instant(), period);
        return generateOtp(counter);
    }

    /**
     * Generate OTP code with counter corresponding the given instant (in millis)
     * @param instant
     * @return
     * @throws IllegalStateException
     */
    public String generateOtpAtInstant(final Instant instant) throws IllegalStateException {
        long counter = calculateTimeBasedCounter(instant, period);
        return generateOtp(counter);
    }

    /**
     * Generate OTP code with the given counter
     * @param counter
     * @return
     * @throws IllegalStateException
     */
    public String generateOtp(final long counter) throws IllegalStateException {
        if (counter < 0)
            throw new IllegalArgumentException("Counter must be greater than or equal to 0");

        Base32 codec = new Base32();
        byte[] secretBytes = codec.decode(secret);
        byte[] counterBytes = ByteBuffer.allocate(Long.BYTES).putLong(counter).array();

        byte[] hash;

        try {
            hash = generateHash(secretBytes, counterBytes);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            throw new IllegalStateException();
        }

        return getCodeFromHash(hash);
    }


    /**
     * Verify the given OTP code with a delay window
     * @param otp
     * @param delayWindow
     * @return
     */
    public boolean verifyOtp(final String otp, final int delayWindow) {
        if (otp.length() != passwordLength) return false;

        long counter = calculateTimeBasedCounter(clock.instant(), period);

        for (int i = -delayWindow; i <= delayWindow; i++) {
            String currentCode = generateOtp(counter + i);
            if (otp.equals(currentCode)) return true;
        }
        return false;
    }

}
