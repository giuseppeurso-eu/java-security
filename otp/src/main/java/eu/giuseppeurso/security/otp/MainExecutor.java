package eu.giuseppeurso.security.otp;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;


public class MainExecutor {

    private static URI OTP_AUTH_URI;
    private static int OTP_DELAY_WINDOW;

    public static void main( String[] args ) throws URISyntaxException, IOException {

        // Uncomment this if you run main from your IDE
        //InputStream conf = FileUtils.openInputStream(new File("src/main/resources/gu-otp.properties"));
        InputStream conf = FileUtils.openInputStream(new File("gu-otp.properties"));
        ResourceBundle settings = new PropertyResourceBundle(conf);

        OTP_AUTH_URI = new URI(settings.getString("otp.authUri"));
        OTP_DELAY_WINDOW=Integer.parseInt(settings.getString("otp.delayWindow"));

        printUsage();

        if(args.length==1 ){
            if(args[0].equalsIgnoreCase("-p")){
                runPairSecretAndVerifiyOtp();
            }
            if(args[0].equalsIgnoreCase("-r")){
                runRegisterOtpAuthApp();
            }
            
        }else{
            runVerifyOtp(null);
        }

    }

    /**
     *
     */
    private static void printUsage() {
        System.out.println("\033[1m==================\033[0m");
        System.out.println("\033[1mHELP\033[0m");
        System.out.println("\033[1m==================\033[0m");
        System.out.println("");

        System.out.println("\033[1m MODE 1. VERIFY OTP USING AUTH-URI STORED IN THE PROPERTIES FILE\033[0m");
        System.out.println("> java -jar gu-opt-exe.jar");
        System.out.println("DEFAULT URI (see gu-otp.properties): "+OTP_AUTH_URI);
        System.out.println("");

        System.out.println("\033[1m MODE 2. PROVIDE AUTH-URI AND VERIFY OTP\033[0m");
        System.out.println("> java -jar gu-opt-exe.jar -p");
        System.out.println("");

        System.out.println("\033[1m MODE 3. REGISTER OTP AUTH URI\033[0m");
        System.out.println("> java -jar gu-opt-exe.jar -r");
        System.out.println("");

    }

    /**
     *
     * @param otpAuthUri
     */
    private static void runVerifyOtp(URI otpAuthUri){
        BufferedReader input = null;
        String otp = "";
        input = new BufferedReader(new InputStreamReader(System.in));

        try {

            while (true) {
                System.out.print("Insert OTP: ");
                otp = input.readLine();
                if (StringUtils.isNotEmpty(otp)) {
                    break;
                }
                System.out.println("Please insert a not empty value");
            }
            System.out.println("Trying to verify otp..."+otp);

            if(otpAuthUri==null){
                otpAuthUri=OTP_AUTH_URI;
            }
            OTPGenerator otpGenerator = OTPGenerator.fromURI(otpAuthUri);
            boolean isGood = otpGenerator.verifyOtp(otp, OTP_DELAY_WINDOW);
            if(isGood){
                System.out.println("OTP Successfully Verified..."+otp);
            }else{
                System.out.println("OTP Verification Failed..."+otp);
            }
        } catch (Exception e) {
            System.out.println("Reading error!");
            System.exit(1);
        }
    }

    /**
     *
     */
    private static void runPairSecretAndVerifiyOtp(){
        BufferedReader input = null;
        String value;
        input = new BufferedReader(new InputStreamReader(System.in));

        try {

            while (true) {
                System.out.print("Insert OTP Auth URI: ");
                value = input.readLine();
                if (StringUtils.isNotEmpty(value)) {
                    break;
                }
                System.out.println("Please insert a not empty value");
            }
            System.out.println("URI..."+value);
            runVerifyOtp(new URI(value));

        } catch (Exception e) {
            System.out.println("Reading error!");
            System.exit(1);
        }
    }

    /**
     *
     */
    private static void runRegisterOtpAuthApp() {
        BufferedReader input = null;
        Map<String, String> paramMap = new HashMap<>();
        String value;
        input = new BufferedReader(new InputStreamReader(System.in));

        try {
            while (true) {
                System.out.print("Insert 'issuer' name: ");
                value = input.readLine();
                if (StringUtils.isNotEmpty(value)) {
                    paramMap.put("issuer", value);
                    break;
                }
                System.out.println("Please insert a not empty value");
            }
            System.out.println("ISSUER..." + paramMap.get("issuer"));


            while (true) {
                System.out.print("Insert 'account' name: ");
                value = input.readLine();
                if (StringUtils.isNotEmpty(value)) {
                    paramMap.put("account", value);
                    break;
                }
                System.out.println("Please insert a not empty value");
            }
            System.out.println("ACCOUNT..." + paramMap.get("account"));

            int secretSize = 160;
            byte[] secret = OTPGenerator.generate(secretSize);
            OTPGenerator generator = new OTPGenerator(secret);
            URI authUri = generator.createOtpAuthURI(paramMap.get("issuer"), paramMap.get("account"));
            if (StringUtils.isNotEmpty(authUri.toString())) {
                System.out.println("OTP AUTH-URI Successfully created!");
                System.out.println("Put the following URL to online QRCode generator and register the Authenticator app on your device: ");
                System.out.println("");
                System.out.println(authUri);
                System.out.println("");
            } else {
                System.out.println("OTP AUTH-URI creation Failed.");
            }
        } catch (Exception e) {
            System.out.println("Reading error!");
            System.exit(1);
        }
    }
    }
